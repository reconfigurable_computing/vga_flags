library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity vga_flags is
 port
 (
		SW	           	: in std_logic_vector (9 downto 0);
		MAX10_CLK1_50	: in std_logic;
		
		VGA_R 			: out std_logic_vector (3 downto 0);
		VGA_G				: out std_logic_vector (3 downto 0);
		VGA_B				: out std_logic_vector (3 downto 0);
		VGA_HS			: out std_logic;
		VGA_VS			: out std_logic
 );
end entity vga_flags;

architecture behav of vga_flags is

signal clk, rgb_sync : std_logic;
signal flag_num : std_logic_vector (2 downto 0);

component pll
	port
	(
		inclk0		: IN STD_LOGIC  := '0';
		
		c0				: OUT STD_LOGIC 
	);
end component;

component vert_fsm
	port
	(
		CLK		: in std_logic;
		RST		: in std_logic;
		
		RGB_SYNC : out std_logic;
		VERT_SYNC: out std_logic
	
	);
end component;

component horz_fsm
	port
	(
		RGB_SYNC : in std_logic;
		RST		: in std_logic;
		CLK		: in std_logic;
		FLAG_NUM	: in std_logic_vector (2 downto 0);
		
		VGA_R		: out std_logic_vector (3 downto 0);
		VGA_G		: out std_logic_vector (3 downto 0);
		VGA_B		: out std_logic_vector (3 downto 0);
		HORZ_SYNC: out std_logic
	);
end component;

component acquireFsm
	port 
	(
		CLK : in std_logic;
		RST : in std_logic;
		ARE_DATA : in std_logic;

		FLAG_NUM : out std_logic_vector (2 downto 0)
	);
end component;

begin

my_pll : pll
	port map
	(
		inclk0 => MAX10_CLK1_50,
		
		c0	=> clk
	);

my_vert_fsm : vert_fsm
	port map
	(
		CLK => clk,
		RST => SW(0),
		
		RGB_SYNC => rgb_sync,
		VERT_SYNC => VGA_VS
	
	);

my_horz_fsm : horz_fsm
	port map
	(
		RGB_SYNC => rgb_sync,
		RST => SW(0),
		CLK => clk,
		FLAG_NUM => flag_num,
		
		VGA_R	=> VGA_R,
		VGA_G	=> VGA_G,
		VGA_B => VGA_B,
		HORZ_SYNC => VGA_HS
	);
	
my_acquirefsm : acquireFsm
	port map
	(
		CLK => clk,
		RST => SW(0),
		ARE_DATA => SW(1),

		FLAG_NUM => flag_num
	);


end architecture;