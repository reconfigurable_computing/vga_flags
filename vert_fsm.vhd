library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity vert_fsm is
	port
	(
		CLK		: in std_logic;
		RST		: in std_logic;
		
		RGB_SYNC : out std_logic;
		VERT_SYNC: out std_logic
	
	);
end entity vert_fsm;

architecture behav of vert_fsm is

type state_type is (reset, v_front_porch, v_sync, v_back_porch, v_pixels);
signal cstate, nstate : state_type;

signal count, ncount : natural := 0;


begin
	sync_proc : process (CLK,RST)
	begin		
		if rising_edge(CLK) then
			if RST = '1' then
				count <= 0;
				cstate <= reset;
			else
				count <= ncount;
				cstate <= nstate;
			end if;
		end if;
	end process;
	
	async_proc : process (count,cstate)
	begin
		case cstate is
			when reset => 
				ncount <= 0;
				VERT_SYNC <= '1';
				RGB_SYNC <= '0';
				if RST = '1' then
					nstate <= reset;
				else
					nstate <= v_front_porch;
				end if;
			
			when v_front_porch =>
				ncount <= count + 1;
				VERT_SYNC <= '1';
				RGB_SYNC <= '0';
				if count < 7999 then
					nstate <= v_front_porch;
				else
					nstate <= v_sync;
					ncount <= 0;
				end if;
			
			when v_sync =>
				ncount <= count + 1;
				VERT_SYNC <= '0';
				RGB_SYNC <= '0';
				if count < 1599 then
					nstate <= v_sync;
				else
					nstate <= v_back_porch;
					ncount <= 0;
				end if;
				
			when v_back_porch =>
				ncount <= count + 1;
				VERT_SYNC <= '1';
				RGB_SYNC <= '0';
				if count < 26399 then
					nstate <= v_back_porch;
				else
					nstate <= v_pixels;
					ncount <= 0;
				end if;
				
			when v_pixels =>
				ncount <= count + 1;
				VERT_SYNC <= '1';
				RGB_SYNC <= '1';
				if count < 383999 then
					nstate <= v_pixels;
				else
					nstate <= v_front_porch;
					ncount <= 0;
				end if;
				
			when others =>
				ncount <= count;
				VERT_SYNC <= '1';
				RGB_SYNC <= '0';					
		end case;
	end process;
end architecture;