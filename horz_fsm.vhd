library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity horz_fsm is
	port
	(
		RGB_SYNC : in std_logic;
		RST		: in std_logic;
		CLK		: in std_logic;
		FLAG_NUM	: in std_logic_vector (2 downto 0);
		
		VGA_R		: out std_logic_vector (3 downto 0);
		VGA_G		: out std_logic_vector (3 downto 0);
		VGA_B		: out std_logic_vector (3 downto 0);
		HORZ_SYNC: out std_logic
	);
end entity horz_fsm;


architecture behav of horz_fsm is

type RAM is array (0 to 7) of std_logic_vector (3 downto 0);
constant lut_r_1 : RAM := (X"0", X"0", X"0", X"0", X"0", X"0", X"0", X"F");
constant lut_r_2 : RAM := (X"F", X"F", X"F", X"F", X"F", X"F", X"F", X"F");
constant lut_r_3 : RAM := (X"F", X"F", X"F", X"F", X"F", X"F", X"0", X"0");

constant lut_g_1 : RAM := (X"0", X"F", X"F", X"0", X"F", X"0", X"F", X"A");
constant lut_g_2 : RAM := (X"F", X"F", X"F", X"F", X"F", X"F", X"F", X"F");
constant lut_g_3 : RAM := (X"0", X"0", X"A", X"0", X"0", X"0", X"F", X"F");

constant lut_b_1 : RAM := (X"F", X"0", X"0", X"0", X"0", X"F", X"0", X"0");
constant lut_b_2 : RAM := (X"F", X"F", X"F", X"0", X"0", X"0", X"F", X"F");
constant lut_b_3 : RAM := (X"0", X"0", X"0", X"0", X"0", X"0", X"0", X"0");

type state_type is (reset, h_front_porch, h_sync, h_back_porch, h_pixels);
signal cstate, nstate : state_type;

signal count, ncount, screen, nscreen : natural := 0;


begin
	sync_proc : process (CLK,RST)
	begin	
		if rising_edge(CLK) then
			if RST = '1' then
				count <= 0;
				cstate <= reset;
			else
				count <= ncount;
				screen <= nscreen;
				cstate <= nstate;
			end if;
		end if;
	end process;

	async_proc : process (count,cstate)
	begin
		case cstate is
			when reset =>
				HORZ_SYNC <= '1';
				ncount <= 0;
				nscreen <= 0;
				VGA_R <= (others => '0');
				VGA_G <= (others => '0');
				VGA_B <= (others => '0');
				if RST = '1' then
					nstate <= reset;
				else
					nstate <= h_front_porch;
				end if;
				
			when h_front_porch =>
				ncount <= count + 1;
				nscreen <= 0;
				HORZ_SYNC <= '1';
				VGA_R <= (others => '0');
				VGA_G <= (others => '0');
				VGA_B <= (others => '0');
				if count < 15 then
					nstate <= h_front_porch;
				else 
					nstate <= h_sync;
					ncount <= 0;
				end if;
				
			when h_sync =>
				ncount <= count + 1;
				nscreen <= 0;
				HORZ_SYNC <= '0';
				VGA_R <= (others => '0');
				VGA_G <= (others => '0');
				VGA_B <= (others => '0');
				if count < 95 then
					nstate <= h_sync;
				else
					nstate <= h_back_porch;
					ncount <= 0;
				end if;
				
			when h_back_porch =>
				ncount <= count + 1;
				nscreen <= 0;
				HORZ_SYNC <= '1';
				VGA_R <= (others => '0');
				VGA_G <= (others => '0');
				VGA_B <= (others => '0');
				if count < 47 then
					nstate <= h_back_porch;
				else
					nstate <= h_pixels;
					ncount <= 0;
				end if;
				
			when h_pixels =>
				ncount <= count + 1;
				HORZ_SYNC <= '1';
				if RGB_SYNC = '1' then
					nscreen <= screen + 1;
					if screen < 213 then
						VGA_R <= lut_r_1(to_integer(unsigned(FLAG_NUM)));
						VGA_G <= lut_g_1(to_integer(unsigned(FLAG_NUM)));
						VGA_B <= lut_b_1(to_integer(unsigned(FLAG_NUM)));
					elsif screen < 426 then
						VGA_R <= lut_r_2(to_integer(unsigned(FLAG_NUM)));
						VGA_G <= lut_g_2(to_integer(unsigned(FLAG_NUM)));
						VGA_B <= lut_b_2(to_integer(unsigned(FLAG_NUM)));
					else
						VGA_R <= lut_r_3(to_integer(unsigned(FLAG_NUM)));
						VGA_G <= lut_g_3(to_integer(unsigned(FLAG_NUM)));
						VGA_B <= lut_b_3(to_integer(unsigned(FLAG_NUM)));
					end if;
				end if;
				if count < 639 then
					nstate <= h_pixels;
				else
					nstate <= h_front_porch;
					ncount <= 0;
					nscreen <= 0;
				end if;
				
			when others =>
				ncount <= count;
				nscreen <= screen;
				HORZ_SYNC <= '1';
				VGA_R <= (others => '0');
				VGA_G <= (others => '0');
				VGA_B <= (others => '0');
		end case;
	end process;
end architecture;