library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity acquireFsm is
	port 
	(
		CLK : in std_logic;
		RST : in std_logic;
		ARE_DATA : in std_logic;

		FLAG_NUM : out std_logic_vector (2 downto 0)
	);
end entity acquireFsm;

architecture Behavioral of acquireFsm is
	type state_type is (idle, debounce_press, accumulate, wait_for_release, debounce_release);
	signal cstate, nstate : state_type;
	
	signal sum, nsum : unsigned (2 downto 0);
	signal count, ncount : natural;

begin
	process(CLK)
	begin
		if rising_edge(CLK) then
			if RST = '1' then
				cstate <= idle;
				sum <= (others => '0');
				count <= 0;
			else
				cstate <= nstate;
				sum <= nsum;
				count <= ncount;
				FLAG_NUM <= std_logic_vector(sum);
			end if;
		end if;
	end process;
	
	process(cstate, sum, count, ARE_DATA)
	begin
	
		case cstate is
			when idle =>
				ncount <= 0;
				nsum <= sum;
				if ARE_DATA = '1' then
					nstate <= debounce_press;
				else
					nstate <= idle;
				end if;
				
			when debounce_press =>
				ncount <= count + 1;
				nsum <= sum;
				if count = 50000 then
					nstate <= accumulate;
				else
					nstate <= debounce_press;
				end if;
				
			when accumulate =>
				ncount <= 0;
				nsum <= sum + "1";
				nstate <= wait_for_release;
				
			when wait_for_release =>
				ncount <= 0;
				nsum <= sum;
				if ARE_DATA = '0' then
					nstate <= debounce_release;
				else
					nstate <= wait_for_release;
				end if;
				
			when debounce_release =>
				ncount <= count + 1;
				nsum <= sum;
				if count = 50000 then
					nstate <= idle;
				else
					nstate <= debounce_release;
				end if;
				
			when others =>
				ncount <= count;
				nsum <= sum;
				nstate <= idle;
		end case;
	end process;

end architecture;